describe('Grid ', function() {
	it('should have living cells', function() {
		
		// x,y, position of living cells
		var grid = new Grid(3,4, ['1,2', '2,3']);

		expect(grid.valueAt(new Point(0,0))).toEqual(0);
		expect(grid.valueAt(new Point(0,1))).toEqual(0);
		expect(grid.valueAt(new Point(1,2))).toEqual(1);
		expect(grid.valueAt(new Point(2,3))).toEqual(1);
	});
});

describe('Grid: isInside ', function() {
	it('should determine if point inside grid', function() {
		
		// width, height
		var grid = new Grid(4,5);

		expect(grid.isInside(new Point(0,0))).toEqual(true);
		expect(grid.isInside(new Point(2,3))).toEqual(true);
		expect(grid.isInside(new Point(-1,-1))).toEqual(false);
		expect(grid.isInside(new Point(33,48))).toEqual(false);
	});
});


describe('GameOfLife: getCellNextStateByNumberOfSurroundingLivingCells ', function() {
	it('should return next state of the cell', function() {
		
		// width, height
		var gameOfLife = new GameOfLife(4,5);

		// getCellNextStateByNumberOfSurroundingLivingCells(numberOfSurroundingLivingCells, currentCellStatus)
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(0, 0)).toEqual(0);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(0, 1)).toEqual(0);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(1, 0)).toEqual(0);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(1, 1)).toEqual(0);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(2, 0)).toEqual(0);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(2, 1)).toEqual(1);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(3, 0)).toEqual(1);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(3, 1)).toEqual(1);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(4, 0)).toEqual(0);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(4, 1)).toEqual(0);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(8, 0)).toEqual(0);
		expect(gameOfLife.getCellNextStateByNumberOfSurroundingLivingCells(8, 1)).toEqual(0);
	});
});

describe('GameOfLife:getNumberOfSurroundingLivingCells', function() {
	it('should return number of surrounding living cells', function() {
		//grid - ['0,0', '0,1', '1,0', '1,1']
		// input arguments x, y, position of living cells
		var gameOfLife = new GameOfLife(2,2, ['0,1', '1,0', '1,1']);
		expect(gameOfLife.getNumberOfSurroundingLivingCells(new Point(0,0))).toEqual(3);
		expect(gameOfLife.getNumberOfSurroundingLivingCells(new Point(0,1))).toEqual(2);
		expect(gameOfLife.getNumberOfSurroundingLivingCells(new Point(1,0))).toEqual(2);
		expect(gameOfLife.getNumberOfSurroundingLivingCells(new Point(1,1))).toEqual(2);
	});
});

describe('GameOfLife:tick', function() {

	it('should not change block state', function() {
		//grid - ['0,0', '0,1', '1,0', '1,1']
		// input arguments x, y, position of living cells
		var gameOfLife = new GameOfLife(2,2, ['0,0','0,1', '1,0', '1,1']);
		
		// L - living cell, Dead cell
		// Block
		expect(gameOfLife.toString()).toEqual('LL\nLL\n');
		gameOfLife.tick();
		expect(gameOfLife.toString()).toEqual('LL\nLL\n');
		gameOfLife.tick();
		expect(gameOfLife.toString()).toEqual('LL\nLL\n');
	});

	it('should oscillate Blinker (period 2)', function() {
		// L - living cell, Dead cell
		// Blinker (period 2)
		var gameOfLife2 = new GameOfLife(3,3, ['1,0', '1,1', '1,2']);
		expect(gameOfLife2.toString()).toEqual('DLD\nDLD\nDLD\n');
		gameOfLife2.tick();
		expect(gameOfLife2.toString()).toEqual('DDD\nLLL\nDDD\n');
		gameOfLife2.tick();
		expect(gameOfLife2.toString()).toEqual('DLD\nDLD\nDLD\n');
	});
});